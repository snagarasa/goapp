package inventoryservice

import (
	"../databasecalls"
	"../report_generation"
	aeroinventory "com/accelaero/aeroinventory/inventory"
	"fmt"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	_ "github.com/lib/pq"
	"os"
)

var blockIds[] string
var blockIdsPerReq int


func onholdBlockedInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {

	reqNum := int(cd.RequestNumber)
	invetBlockTransferReq := &aeroinventory.InventoryBlockTransferRequest{}

	if len(blockIds)<=numberOfRequestsR {
		var arr[] string
		if reqNum<len(blockIds) {
			arr = append(arr, blockIds[reqNum])
		}else {
			arr = append(arr, blockIds[0])
		}
		invetBlockTransferReq.BlockIds = arr

	} else {
		if reqNum != (numberOfRequestsR-1) {
			invetBlockTransferReq.BlockIds = blockIds[(reqNum * blockIdsPerReq):((reqNum * blockIdsPerReq) + blockIdsPerReq)]
		} else {
			invetBlockTransferReq.BlockIds = blockIds[(reqNum * blockIdsPerReq):]
		}
	}


	fmt.Print(reqNum)
	fmt.Print("  ")
	fmt.Print(invetBlockTransferReq.BlockIds)
	fmt.Print("\n")

	binData, err := proto.Marshal(invetBlockTransferReq)


	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func OnholdBlockedInventory(numberOfRequests int, numberOfConcurrentRequests uint)  {

	numberOfRequestsR = numberOfRequests

	blockIds = databasecalls.GetAllBlockIds()

	if len(blockIds) == 0 {
		fmt.Println("No block ids found")
		os.Exit(1)
	}

	blockIdsPerReq = len(blockIds)/numberOfRequests
	if blockIdsPerReq == 0 {
		blockIdsPerReq = 1
	}
	fmt.Print("Block->Onhold Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.InventoryService.onholdBlockedInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/InventoryService.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(onholdBlockedInventoryDataFunc))

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	report_generation.PrintReport(report)
}
