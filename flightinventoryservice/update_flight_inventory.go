package flightinventoryservice

import (
	aeroinventory "com/accelaero/aeroinventory"
	"fmt"
	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"strconv"
)

var flightSegIdG []string
var fccSegInventoryIdG []string
var fccSegBCInventoryIdG []string
var allocatedSeatsG []string
var priorityG []string
var closedG []string

func getFccSegBCInventory(cd *runner.CallData) *aeroinventory.FlightCCSegBCInventoryRQ{

	resFccSegBCInv := cd.RequestNumber % int64(len(fccSegInventoryIdG))
	resAllocSeat := cd.RequestNumber % int64(len(allocatedSeatsG))
	resPrt := cd.RequestNumber % int64(len(priorityG))
	resCls := cd.RequestNumber % int64(len(closedG))
	flightCCSegBCInventory := &aeroinventory.FlightCCSegBCInventoryRQ{}
	fccsegBC, err := strconv.ParseInt(fccSegBCInventoryIdG[resFccSegBCInv], 10, 32)
	flightCCSegBCInventory.FccSegBCInventoryId  = int32(fccsegBC)
	flightCCSegBCInventory.BookingClassCode = "C1"
	allocseat, err := strconv.ParseInt(allocatedSeatsG[resAllocSeat], 10, 32)
	flightCCSegBCInventory.AllocatedSeats = int32(allocseat)
	prt, err := strconv.ParseBool(priorityG[resPrt])
	flightCCSegBCInventory.Priority = prt
	cls, err := strconv.ParseBool(closedG[resCls])
	flightCCSegBCInventory.Closed = cls

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return flightCCSegBCInventory
}

func getFccSegInventoriesObject(cd *runner.CallData) *aeroinventory.FlightCCSegInventoryRQ{

	resFccSegInv := cd.RequestNumber % int64(len(fccSegInventoryIdG))
	flightCCSegInventory :=&aeroinventory.FlightCCSegInventoryRQ{}
	flightCCSegInventory.FccSegBCInventories = []*aeroinventory.FlightCCSegBCInventoryRQ{getFccSegBCInventory(cd)}
	fccseg, err := strconv.ParseInt(fccSegInventoryIdG[resFccSegInv], 10, 32)
	flightCCSegInventory.FccSegInventoryId = int32(fccseg)
	flightCCSegInventory.CurtailOversellCount = 0
	flightCCSegInventory.CurtailOversellFlag = "CURTAIL"

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return flightCCSegInventory
}

func getFlightSegmentObject(cd *runner.CallData) *aeroinventory.FlightSegInventoryRQ{

	resFlightSegId := cd.RequestNumber % int64(len(flightSegIdG))
	flightSegInventory := &aeroinventory.FlightSegInventoryRQ{}
	flightSegInventory.FccSegInventories = []*aeroinventory.FlightCCSegInventoryRQ{getFccSegInventoriesObject(cd)}
	fs, err := strconv.ParseInt(flightSegIdG[resFlightSegId], 10, 32)
	flightSegInventory.FlightSegId = int32(fs)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return flightSegInventory
}

func getUpdateFlightInventoryDataFun(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte{

	resFlightId := cd.RequestNumber % int64(len(flightIdG))
	flightInventoryUpdateRQ  := &aeroinventory.FlightInventoryUpdateRQ{}
	fi, err := strconv.ParseInt(flightIdG[resFlightId], 10, 32)
	flightInventoryUpdateRQ.FlightId  = int32(fi)
	flightInventoryUpdateRQ.FlightSegments = []*aeroinventory.FlightSegInventoryRQ{getFlightSegmentObject(cd)}
	binData, err := proto.Marshal(flightInventoryUpdateRQ)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}


func UpdateFlightInventory(numberOfRequests int,
	numberOfConcurrentRequests uint,
	flightId []string,
	flightSegId []string,
	fccSegInventoryId []string,
	fccSegBCInventoryId []string,
	allocatedSeats []string,
	priority []string,
	closed []string){

	flightIdG = flightId
	flightSegIdG = flightSegId
	fccSegInventoryIdG = fccSegInventoryId
	fccSegBCInventoryIdG = fccSegBCInventoryId
	allocatedSeatsG = allocatedSeats
	priorityG = priority
	closedG = closed

	report, err := runner.Run(
		"aeroinventory.FlightInventoryService.updateFlightInventory",
		"localhost:6565",
		runner.WithProtoFile("/home/chethan/Codebase/goapp-local/goProtos/proto/flight_inventory_allocation.proto", []string{}),
		runner.WithInsecure(true),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(getUpdateFlightInventoryDataFun),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}
	printer.Print("pretty")

}
