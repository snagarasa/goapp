package structs

type GoShowData struct{

	NumberOfRequests int64 `json:"numberOfRequests"`
	FlightNumberStartsWith int64 `json:"flightNumberStartsWith"`
	DepartureDateAndTimeLocal string `json:"departureDateAndTimeLocal"`
	BookingClassCode string `json:"bookingClassCode"`
	ChildCapacity int32 `json:"childCapacity"`
	InfantCapacity int32 `json:"infantCapacity"`
	AdultCapacity int32 `json:"adultCapacity"`
	NumberOfFlightsPerReq int64 `json:"numberOfFlightsPerReq"`
}