package structs

type RfToCancelData struct {
	NumberOfRequestsForRF int `json:"numberOfRequestsForRF"`
	NumberOfConcurrentRequestsForRF uint `json:"numberOfConcurrentRequestsForRF"`
	SourceFlightId int32 `json:"sourceFlightId"`
	DateRangeStartingDateForRF string `json:"dateRangeStartingDateForRF"`
	DateRangeSize int `json:"dateRangeSize"`

	FlightNumbers[] string `json:"flightNumbers"`

	NumberOfRequestsForOnholdSellCancelBlock int `json:"numberOfRequestsForOnholdSellCancelBlock"`
	NumberOfConcurrentRequestsForOnholdSellCancelBlock uint `json:"numberOfConcurrentRequestsForOnholdSellCancelBlock"`
	BookingClass string `json:"bookingClass"`
	ChildCapacities[] string `json:"childCapacities"`
	InfantCapacities[] string `json:"infantCapacities"`
	AdultCapacities[] string `json:"adultCapacities"`
	DepartureDateLocalStartsWith string `json:"departureDateLocalStartsWith"`

	BlockTimePeriod string `json:"blockTimePeriod"`

	NumberOfRequestsForBlockToOnhold int `json:"numberOfRequestsForBlockToOnhold"`
	NumberOfConcurrentRequestsForBlockToOnhold uint `json:"numberOfConcurrentRequestsForBlockToOnhold"`

}
