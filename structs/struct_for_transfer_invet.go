package structs

type TransferInvetData struct{

	FromFlightNumberStartsWith int64 `json:"fromFlightNumberStartsWith"`
	NumberOfRequests int `json:"numberOfRequests"`
	ToFlightDepartureDateLocalTime string `json:"toFlightDepartureDateLocalTime"`
	FromFlightDepartureDateLocalTime string `json:"fromFlightDepartureDateLocalTime"`
	NumberOfToFlightsPerFlight int64 `json:"numberOfToFlightsPerFlight"`
	BookingClass string `json:"bookingClass"`
	ChildSoldCapacity int32 `json:"childSoldCapacity"`
	InfantSoldCapacity int32 `json:"infantSoldCapacity"`
	AdultSoldCapacity int32 `json:"adultSoldCapacity"`
	ChildOnhCapacity int32 `json:"childOnhCapacity"`
	InfantOnhCapacity int32 `json:"infantOnhCapacity"`
	AdultOnhCapacity int32 `json:"adultOnhCapacity"`
}