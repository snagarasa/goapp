package main

import (
	"../goshowservice"
	"../structs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func InvokeGoShow(){

	var goShowData structs.GoShowData
	jsonFile, err := os.Open("data/input_go_show.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened data.json")
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &goShowData)

	goshowservice.GoShow(
		goShowData.NumberOfRequests,
		goShowData.FlightNumberStartsWith,
		goShowData.DepartureDateAndTimeLocal,
		goShowData.BookingClassCode,
		goShowData.AdultCapacity,
		goShowData.ChildCapacity,
		goShowData.InfantCapacity,
		goShowData.NumberOfFlightsPerReq)

}
