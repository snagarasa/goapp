package flightservice

import (
	aeroinventory "com/accelaero/aeroinventory"
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"

	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
)

func getFlightNumbersDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {
	msg := &aeroinventory.FlightNumbersSearchRQ{}
	msg.AircraftModel = "A320"
	msg.Destination = "SHJ"
	msg.Origin = "CMB"
	msg.FutureSearch = true

	binData, err := proto.Marshal(msg)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func GetFlightNumbersSendRequest(numberOfRequests int, numberOfConcurrentRequests uint)  {

	report, err := runner.Run(
		"aeroinventory.FlightService.getFlightNumbers",
		"localhost:6565",
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(getFlightNumbersDataFunc),
		
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/flight.proto",
			[]string{}),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}

	printer.Print("pretty")

}
